//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by treeview.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TREEVIEW_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDD_TITLETEMPLATE_SET           129
#define IDD_TITLETEMPLATE_SHOW          130
#define IDC_MFCSHELLLIST1               1001
#define IDC_TREE1                       1002
#define IDC_EDIT1                       1003
#define IDC_BUTTON1                     1004
#define IDC_TITLESET_LITTLEALPHA        1005
#define IDC_TITLESET_NUM                1006
#define IDC_TITLESET_SAMECHAR           1008
#define IDC_TITLESET_ALLSTRING          1009
#define IDC_TITLESET_TITLECONTENT       1014
#define IDC_TITLESET_TITLESHOW          1015
#define IDC_TITLESET_NEXTLINE           1019
#define IDC_TITLESET_SPACE              1020
#define IDC_CANCEL                      1021
#define IDC_TITLESET_RESET              1022
#define IDC_TITLESET_ALLTITLE           1023
#define IDC_TITLESET_INSERETBUTTON      1024
#define IDC_TITLESET_FIRSTTITLE         1025
#define IDC_TITLESET_PRETITLE           1026
#define IDC_TITLESET_NEXTTITLE          1027
#define IDC_TITLESET_INSERTTITLE        1028
#define IDC_TITLESET_CLEARTITLE         1029
#define IDC_TITLESET_BIGALPHA           1030
#define IDC_TITLESET_INSERTCHAR         1031
#define IDC_STATIC_TITLETAG             1032
#define IDC_STATIC_TITLENUMSHOW         1033
#define IDC_TITLESET_CANCELTITLE        1034
#define IDC_SHOWTITLE_STATICSHOW        1035
#define IDC_SHOW                        1036
#define IDC_SHOWTITLE_GROUPS            1036
#define IDC_BUTTON2                     1037
#define IDC_REANALYSE                   1037

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1038
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
